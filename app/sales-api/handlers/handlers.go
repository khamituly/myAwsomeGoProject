// Package handlers contains the full set of handler functions and routes
// supported by the web api.
package handlers

import (
	_ "github.com/dimfeld/httptreemux/v5"
	"gitlab.com/khamituly/myAwsomeGoProject/buisness/mid"
	"gitlab.com/khamituly/myAwsomeGoProject/foundation/web"
	"log"
	"net/http"
	"os"
)

func API(build string, shutdown chan os.Signal, log *log.Logger) *web.App {

	app := web.NewApp(shutdown, mid.Logger(log),mid.Errors(log),mid.Panics(log))

	check := check{
		log: log,
	}

	app.Handle(http.MethodGet, "/readiness", check.readiness)

	return app
}
