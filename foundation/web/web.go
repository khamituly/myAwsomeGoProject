// Package web contains a small web framework extension.
package web

import (
	"context"
	"github.com/dimfeld/httptreemux/v5"
	"net/http"
	"os"
	"syscall"
	"time"
	"github.com/google/uuid"
)

// ctxKey represents the type of value for the context key.
type ctxKey int

// KeyValues is how request values are stored/retrieved.
const KeyValues ctxKey = 1

// Values represent state for each request.
type Values struct {
	TraceID    string
	Now        time.Time
	StatusCode int
}

// App...
type App struct {
	*httptreemux.ContextMux
	shutdown chan os.Signal
	mw 		   []Middleware
}

func ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	panic("implement me")
}

func NewApp(shutdown chan os.Signal, mw ...Middleware) *App {
	app := App{
		ContextMux: httptreemux.NewContextMux(),
		shutdown: 		shutdown,
		mw: 			mw,
	}
	return &app
}

// A Handler is a type that handles an http request within our own little mini
// framework.
type Handler func(ctx context.Context, w http.ResponseWriter, r *http.Request) error


//Handle...
func (a *App) Handle(method string, path string,handler Handler,mw ...Middleware) {

	// First wrap handler specific middleware around this handler.
	handler = wrapMiddleware(mw, handler)

	// Add the application's general middleware to the handler chain.
	handler = wrapMiddleware(a.mw, handler)

	h := func(w http.ResponseWriter, r *http.Request){

		// Set the context with the required values to
		// process the request.
		v := Values{
			TraceID: uuid.New().String(),
			Now:     time.Now(),
		}

		ctx := context.WithValue(r.Context(), KeyValues, &v)

		if err := handler(ctx,w,r); err != nil{
			a.SignalShutdown()
			return
		}


	}
	a.ContextMux.Handle(method,path,h)
}




func (a *App) SignalShutdown() {
	a.shutdown <- syscall.SIGTERM
}